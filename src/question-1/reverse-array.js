let source = ['Apple', 'Banana', 'Orange', 'Coconut'];
function reverseArray(source, length) {
    for (let i = 0; i < length; i++) {
        let item = source.shift();
        source.splice(length -i, 0, item);
    }
    return source;
}
console.log(reverseArray(source, source.length -1));