import { mount, flushPromises } from '@vue/test-utils'
import App  from '@/App.vue'
import FactCard from '@/components/FactCard'
import axios from 'axios'

const mockCatFacts = {
      data: {
        data: [
          {
            fact: 'Cats have nine lives.',
            length: 4
          },
          {
            fact: 'A cat is not a dog.',
            length: 100
          },
          {
            fact: 'Big cats roar.',
            length: 60
          }
        ]
      }
    }

jest.spyOn(axios, 'get').mockResolvedValue(mockCatFacts)

describe('App renders on load', () => {

  test('renders fact cards in order', async () => {
    // Arrange.
    const wrapper = mount(App)

    await flushPromises()

    const cards = wrapper.findAll('article')

    // Assert.
    expect(axios.get).toHaveBeenCalledTimes(1)
    expect(cards).toHaveLength(3)
    expect(cards[0].text()).toContain('A cat is not a dog.')
    expect(cards[1].text()).toContain('Big cats roar.')
    expect(cards[2].text()).toContain('Cats have nine lives.')
  });

});

