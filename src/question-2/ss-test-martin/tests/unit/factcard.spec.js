import { render } from '@testing-library/vue'
import FactCard from '@/components/FactCard.vue'

describe('FactCard.vue', () => {
  it('renders a fact', () => {
    const fact = 'this is a fact'
    const { getByText } = render(FactCard, {
      props: { fact }
    })
    getByText(fact)
  })

  it('renders card number', () => {
    const index = 7
    const { getByText } = render(FactCard, {
      props: { index }
    })
    getByText(`#${index}`)
  })
})